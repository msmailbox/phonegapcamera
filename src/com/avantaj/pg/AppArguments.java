package com.avantaj.pg;

import org.json.JSONArray;
import org.json.JSONObject;

public class AppArguments {

	private String folderName = "Pictures";

	private String filePrefix = "Default";

	private int intraval = 3;

	public AppArguments(JSONArray jsonArr) {
		
		JSONObject jobj = null;
		
		
		try {
			jobj = jsonArr.getJSONObject(0);	
		} catch (Exception e) {
			return;
		}

		try {
			folderName = jobj.getString("folder");
		} catch (Exception e) {
			folderName = "Pictures";
		}

		try {
			filePrefix = jobj.getString("fileprefix");
		} catch (Exception e) {
			filePrefix = "Default";
		}
		
		try {
			intraval = jobj.getInt("intravel");
		} catch (Exception e) {
			intraval = 3;
		}
	}

	public String getFolderName() {
		return folderName;
	}

	public String getFilePrefix() {
		return filePrefix;
	}

	public int getIntraval() {
		return intraval;
	}
}
