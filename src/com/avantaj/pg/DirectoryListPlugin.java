package com.avantaj.pg;

import org.apache.cordova.api.PluginResult;
import org.json.JSONArray;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import com.avantaj.pg.camera.UsingCameraAPIActivity;
import com.phonegap.api.Plugin;

public class DirectoryListPlugin extends Plugin {

	private String callbackId  = null;	
	@Override
	public PluginResult execute(String action, JSONArray data, String callbackId) {
	
		//TODO set image quality and auto focus
		
		Log.w(">>>>>", action);
		Log.w(">>>>>", data.toString());
		
		AppArguments appArgs = new AppArguments(data);
		
		this.callbackId = callbackId;
		Intent intent = new Intent(this.ctx.getContext(), UsingCameraAPIActivity.class);
		intent.putExtra("folder", appArgs.getFolderName());
		intent.putExtra("fileprefix", appArgs.getFilePrefix());
		intent.putExtra("intravel", appArgs.getIntraval());
		
		this.ctx.startActivityForResult((Plugin) this, intent,1);
		PluginResult pr = new PluginResult(PluginResult.Status.NO_RESULT);
		pr.setKeepCallback(true);
		return pr;
		
	}
	
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		
		super.onActivityResult(requestCode, resultCode, intent);
		Log.w("GOT JSON RESULT",intent.getStringExtra("result"));
		
		String cid = callbackId;
		PluginResult pr = null;
		Log.w("Error code", ""+resultCode);
		if(resultCode == Activity.RESULT_OK){
			pr = new PluginResult(PluginResult.Status.OK, intent.getStringExtra("result"));
			pr.setKeepCallback(true);
			this.success(pr, cid);
		}else{
			pr = new PluginResult(PluginResult.Status.ERROR, intent.getStringExtra("result"));
			pr.setKeepCallback(true);
			this.error(pr, cid);
		}
	}
}
